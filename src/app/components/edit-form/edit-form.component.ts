import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { IWords } from './../../interfaces/api.interface';
import { ApiService } from './../../services/api.service';

@Component({
  selector: 'app-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.scss']
})
export class EditFormComponent implements OnInit {
  word: IWords;

  constructor(
    private apiService: ApiService,
    private activeRouter: ActivatedRoute,
    private router: Router
  ) {
    this.word = null;
  }

  ngOnInit() {
    // Get the route id when init and link the word value to the service word
    this.activeRouter.paramMap.subscribe(params => {
      const id = params.get('id');
      this.word = this.apiService.getwordById(id);
    });
  }

  // Edit the word and navigate home after it resolves
  onFormSubmit(word: IWords) {
    this.apiService.setEditWord(word).then(() => {
      this.router.navigate(['/']);
    });
  }
}

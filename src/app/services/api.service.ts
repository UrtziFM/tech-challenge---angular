import { IWords } from './../interfaces/api.interface';
import { Injectable } from '@angular/core';
// import { HttpClient } from '@angular/common/http';

import { getApiResponse } from './../api/api-mock';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public words: IWords[];

  constructor(/* private http: HttpClient */) {
    this.words = [];
    this.getInitialResponse();
  }

  public getInitialResponse(): void {
    // If we bring data from a service, we should use the http method and subscribe to the observable
    // this.http.get().subscribe()

    // Instead, we use a normal promise to simulate the API behaviour
    getApiResponse().then((response: IWords[]) => {
      response.forEach((word: IWords) => {
        this.words.push(word);
      });
    });
  }

  public getWords(): IWords[] {
    return this.words;
  }

  private checkIds(firstId: string|number, secondId: string|number): boolean {
    return firstId.toString() === secondId.toString()
  }

  // Find a word by id in the array and return it
  public getwordById(id: number|string): IWords {
    return this.words.find((word: IWords) => this.checkIds(word.id, id));
  }

  // Push a new word to the words array
  public setCreateWord(newWord: IWords): Promise<boolean> {
    this.words.push(newWord);

    // Return a promise so we can control when the word is deleted
    return Promise.resolve(true);
  }

  // Find a word wiht the same id of the edited one change it's whole value
  public setEditWord(editedWord: IWords): Promise<boolean> {
    this.words.forEach((word: IWords, index: number) => {
      if (this.checkIds(word.id, editedWord.id)) {
        this.words[index] = editedWord;
      }
    });

    // Return a promise so we can control when the word is deleted
    return Promise.resolve(true);
  }

  // Find the index of a word by its id and remove it from the array with splice
  public setDeleteWord(id: number|string): void {
    const indexOfWord = this.words.findIndex(word => this.checkIds(word.id, id));
    this.words.splice(indexOfWord, 1);
  }
}
